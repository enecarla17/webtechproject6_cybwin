const express = require("express")
const router = express.Router()
const {
	getActivity,
	createActivity,
	getActivities,
	getActivityById
} = require("../controller/activity")
const { getUser, createUser } = require("../controller/user")
const { getReactions } = require("../controller/reaction")
const { createReactionBind, getFeedback } = require("../controller/reactionBind")
const { createSpeciality, getSpecialities } = require("../controller/speciality")

router.get("/activity/:code", getActivity)
router.get("/activityById/:id", getActivityById)
router.get("/activities/:id", getActivities)
router.get("/user/:email", getUser)
router.get("/reactions", getReactions)
router.get("/specialities", getSpecialities)
router.get("/feedback/:idActivity", getFeedback)
router.post("/activity", createActivity)
router.post("/reactionBind", createReactionBind)
router.post("/user", createUser)
router.post("/speciality", createSpeciality)

module.exports = router
