const { Speciality } = require("../models/speciality")

const speciality = {
	getSpecialities: async code => {
		return await Speciality.findAll().then(function(specialities) {
			return specialities
		})
	},
	create: async speciality => {
		try {
			return await Speciality.create(speciality)
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = speciality
