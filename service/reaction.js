const { Reaction } = require("../models/reaction")

const reaction = {
	getReactions: async code => {
		return await Reaction.findAll().then(function(reactions) {
			return reactions
		})
	}
}

module.exports = reaction
