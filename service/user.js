const { User } = require("../models/user")

const user = {
	getUserByEmail: async email => {
		return await User.findOne({
			where: { email: email }
		}).then(function(user) {
			return user
		})
	},
	create: async user => {
		try {
			return await User.create(user)
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = user
