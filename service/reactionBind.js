const { ReactionBind } = require("../models/reactionBind")

const reactionBind = {
	getFeedback: async idActivity => {
		return await ReactionBind.findAll({
			where: { idActivity: parseInt(idActivity) }
		}).then(function(binds) {
			return binds
		})
	},

	create: async reactionBind => {
		try {
			return await ReactionBind.create(reactionBind)
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = reactionBind
