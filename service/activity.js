const { Activity } = require("../models/activity")

const activity = {
	getActivityByCode: async code => {
		return await Activity.findOne({
			where: { code: parseInt(code) }
		}).then(function(activity) {
			return activity
		})
	},
	getActivityById: async id => {
		return await Activity.findOne({
			where: { id: parseInt(id) }
		}).then(function(activity) {
			return activity
		})
	},
	get: async id => {
		return await Activity.findAll({
			where: { idAdmin: parseInt(id) }
		}).then(function(activities) {
			return activities
		})
	},
	create: async activity => {
		try {
			return await Activity.create(activity)
		} catch (err) {
			throw new Error(err.message)
		}
	}
}

module.exports = activity
