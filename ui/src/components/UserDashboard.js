import React from "react"

import axios from "axios"

class UserDashboard extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			code: ""
		}

		this.onPropertyChange = this.onPropertyChange.bind(this)
		this.onSubmitForm = this.onSubmitForm.bind(this)
	}

	onPropertyChange(event) {
		this.setState({
			[event.target.name]: event.target.value
		})
	}

	onSubmitForm() {
		let self = this
		axios.get("http://localhost:8080/api/activityById/" + this.state.code).then(activity => {
			if (activity.data) {
				self.props.history.push("/feedback/" + activity.data.id)
			}
		})
	}

	render() {
		return (
			<div className='mt-4 ml-4'>
				<h2>Welcome!</h2>
				<form className='col-sm-3'>
					<label>Activity code</label>
					<input
						value={this.state.code}
						placeholder='code'
						className='form-control'
						type='text'
						name='code'
						onChange={this.onPropertyChange}
					/>
					<button type='button' className='btn mt-2 btn-sm btn-primary' onClick={this.onSubmitForm}>
						Go
					</button>
				</form>
			</div>
		)
	}
}

export default UserDashboard
