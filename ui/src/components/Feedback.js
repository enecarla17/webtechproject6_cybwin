import React from "react"

import axios from "axios"
let activityIdParam, setIntervalResponse
class Feedback extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			reactions: [],
			activity: {},
			feedback: []
		}

		this.sendReaction = this.sendReaction.bind(this)
	}

	sendReaction(reaction) {
		axios.post("http://localhost:8080/api/reactionBind", {
			idActivity: activityIdParam,
			idReaction: reaction.id,
			date: new Date()
		})
	}

	componentDidMount() {
		const { activityId } = this.props.match.params
		activityIdParam = activityId
		axios.get("http://localhost:8080/api/activityById/" + activityId).then(activity => {
			this.setState({ activity: activity.data })
		})
		axios.get("http://localhost:8080/api/reactions").then(reactions => {
			this.setState({ reactions: reactions.data })
		})
		setIntervalResponse = setInterval(() => {
			axios.get("http://localhost:8080/api/feedback/" + activityIdParam).then(feedback => {
				this.setState({ feedback: feedback.data })
			})
		}, 2000)
	}

	componentWillUnmount = () => {
		clearInterval(setIntervalResponse)
	}

	render() {
		return (
			<div className='ml-2'>
				<h2>{this.state.activity.about}</h2>
				<h4>Timp: {this.state.activity.time}</h4>
				<h4 className='mb-2'>Data: {new Date(this.state.activity.date).toLocaleDateString()}</h4>
				<br></br>
				{this.props.user.isAdmin ? (
					<div>
						<table className='table'>
							<thead>
								<tr>
									<th>Reactie</th>
									<th>Counter</th>
								</tr>
							</thead>
							<tbody>
								{this.state.reactions.map(reaction => (
									<tr>
										<td>{reaction.name}</td>
										<td>
											{
												this.state.feedback.filter(feedback => feedback.idReaction === reaction.id)
													.length
											}
										</td>
									</tr>
								))}
							</tbody>
						</table>
					</div>
				) : (
					this.state.reactions.map(reaction => (
						<button
							className='btn btn-xs btn-primary mr-2'
							onClick={() => this.sendReaction(reaction)}
						>
							{reaction.name}
						</button>
					))
				)}
			</div>
		)
	}
}

export default Feedback
