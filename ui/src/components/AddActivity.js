import React from "react"
import axios from "axios"
import DateTimePicker from "react-datetime-picker"
import { Link } from "react-router-dom"

class AddActivity extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			code: "",
			about: "",
			time: "",
			date: new Date()
		}
		this.onPropertyChange = this.onPropertyChange.bind(this)
		this.onSubmitForm = this.onSubmitForm.bind(this)
	}
	onPropertyChange(event) {
		this.setState({
			[event.target.name]: event.target.value
		})
	}

	async onSubmitForm() {
		const object = {
			code: this.state.code,
			about: this.state.about,
			time: this.state.time,
			date: this.state.date,
			idAdmin: this.props.user.id
		}
		await axios.post("http://localhost:8080/api/activity", object)

		this.props.history.push("/dashboard/professor")
	}
	render() {
		return (
			<div className='col-sm-6'>
				<form>
					<label>Code</label>
					<input
						value={this.state.code}
						className='form-control'
						type='text'
						name='code'
						onChange={this.onPropertyChange}
					/>
					<label>About</label>
					<input
						value={this.state.about}
						className='form-control'
						type='text'
						name='about'
						onChange={this.onPropertyChange}
					/>
					<label>Time</label>
					<input
						value={this.state.time}
						className='form-control'
						type='number'
						name='time'
						onChange={this.onPropertyChange}
					/>
					<br></br>
					<DateTimePicker
						name='date'
						onChange={date => this.setState({ date: new Date(date) })}
						value={this.state.date}
					/>
					<br />
					<button type='button' className='mt-2 btn btn-sm btn-primary' onClick={this.onSubmitForm}>
						Add
					</button>
					<Link to='/dashboard/professor'>
						<button type='button' className='mt-2 btn ml-2 btn-sm btn-warning'>
							Back
						</button>
					</Link>
				</form>
			</div>
		)
	}
}

export default AddActivity
