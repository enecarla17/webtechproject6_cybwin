import React from "react"
import axios from "axios"
import { Link } from "react-router-dom"
class Register extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			firstname: "",
			lastname: "",
			password: "",
			email: "",
			idSpeciality: "",
			isAdmin: false,
			specialities: []
		}
		this.onPropertyChange = this.onPropertyChange.bind(this)
		this.onSubmitForm = this.onSubmitForm.bind(this)
	}
	onPropertyChange(event) {
		this.setState({
			[event.target.name]: event.target.value
		})
	}

	async onSubmitForm() {
		const object = {
			firstname: this.state.firstname,
			lastname: this.state.lastname,
			email: this.state.email,
			password: this.state.password,
			idSpeciality: this.state.idSpeciality,
			isAdmin: this.state.isAdmin
		}
		await axios.post("http://localhost:8080/api/user", object)
		this.props.history.push("/")
	}

	componentDidMount() {
		axios.get("http://localhost:8080/api/specialities").then(specialities => {
			this.setState({ specialities: specialities.data })
		})
	}

	render() {
		return (
			<div className='col-sm-6'>
				<form>
					<label>Firstname</label>
					<input
						value={this.state.firstname}
						className='form-control'
						type='text'
						name='firstname'
						onChange={this.onPropertyChange}
					/>
					<label>Lastname</label>
					<input
						value={this.state.lastname}
						className='form-control'
						type='text'
						name='lastname'
						onChange={this.onPropertyChange}
					/>
					<label>Email</label>
					<input
						value={this.state.email}
						className='form-control'
						type='text'
						name='email'
						onChange={this.onPropertyChange}
					/>
					<label>Password</label>
					<input
						value={this.state.password}
						className='form-control'
						type='password'
						name='password'
						onChange={this.onPropertyChange}
					/>
					<label>Speciality</label>
					<select
						value={this.state.idSpeciality}
						className='form-control'
						name='idSpeciality'
						onChange={this.onPropertyChange}
					>
						{this.state.specialities.map(speciality => (
							<option value={speciality.id}>{speciality.name}</option>
						))}
					</select>
					<label>Admin</label>
					<input
						value={this.state.isAdmin}
						className='form-control'
						type='checkbox'
						style={{ width: 30 + "px" }}
						name='isAdmin'
						onChange={this.onPropertyChange}
					/>
					<br />
					<button type='button' className='btn btn-sm btn-primary' onClick={this.onSubmitForm}>
						Register
					</button>
					<Link to='/'>
						<button type='button' className='btn ml-2 btn-sm btn-warning'>
							Back
						</button>
					</Link>
				</form>
			</div>
		)
	}
}

export default Register
