import React from "react"
import { Link } from "react-router-dom"
class ActivityCard extends React.Component {
	state = {}
	render() {
		return (
			<div class='card ml-3' style={{ width: 16 + "rem" }}>
				<div class='card-body'>
					<h5 class='card-title'>{this.props.activity.about}</h5>
					<h6 class='card-subtitle mb-2 text-muted'>Code: {this.props.activity.code}</h6>
					<p class='card-text'>Date: {new Date(this.props.activity.date).toLocaleDateString()}</p>
					<p class='card-text'>Time: {this.props.activity.time}</p>
					<Link to={"/feedback/" + this.props.activity.id}>
						<button class='btn btn-sm btn-primary'>Details</button>
					</Link>
				</div>
			</div>
		)
	}
}

export default ActivityCard
