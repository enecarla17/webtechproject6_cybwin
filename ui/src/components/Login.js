import React, { Component } from "react"
import { Link } from "react-router-dom"

class LoginView extends Component {
	constructor(props) {
		super(props)

		this.state = {
			email: "",
			password: ""
		}

		this.onPropertyChange = this.onPropertyChange.bind(this)
		this.onSubmitForm = this.onSubmitForm.bind(this)
	}

	onPropertyChange(event) {
		this.setState({
			[event.target.name]: event.target.value
		})
	}

	onSubmitForm() {
		this.props.login(this.state.email, this.state.password, this.props.history)
	}

	render() {
		return (
			<div className='mt-4 ml-4 col-sm-3'>
				<div className='row'>
					<h3>Login</h3>
				</div>
				<div>
					<form>
						<input
							value={this.state.email}
							placeholder='email'
							className='form-control'
							type='text'
							name='email'
							onChange={this.onPropertyChange}
						/>
						<input
							value={this.state.password}
							placeholder='password'
							className='form-control'
							type='password'
							name='password'
							onChange={this.onPropertyChange}
						/>
						<button
							type='button'
							className='btn mt-2 btn-sm btn-primary'
							onClick={this.onSubmitForm}
						>
							Submit
						</button>
						<Link to='/register'>
							<button type='button' className='btn mt-2 ml-2 btn-sm btn-success'>
								Register
							</button>
						</Link>
					</form>
				</div>
			</div>
		)
	}
}

export default LoginView
