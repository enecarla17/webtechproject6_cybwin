import React from "react"

import ActivityList from "./ActivityList"
import { Link } from "react-router-dom"

import axios from "axios"

class ProfessorDashboard extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			activities: []
		}
	}

	componentDidMount() {
		axios.get("http://localhost:8080/api/activities/" + this.props.user.id).then(activities => {
			this.setState({ activities: activities.data })
		})
	}

	render() {
		return (
			<div className='mt-4 ml-4'>
				<h2>Activities</h2>
				<ActivityList activities={this.state.activities}></ActivityList>
				<Link to='/addActivity'>
					<button type='button' className='btn mt-2 btn-sm btn-success'>
						New activity
					</button>
				</Link>
			</div>
		)
	}
}

export default ProfessorDashboard
