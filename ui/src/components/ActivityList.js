import React from "react"

import ActivityCard from "./ActivityCard"

class ActivityList extends React.Component {
	state = {}
	render() {
		return (
			<div className='row'>
				{this.props.activities.map(activity => (
					<ActivityCard activity={activity}></ActivityCard>
				))}
			</div>
		)
	}
}

export default ActivityList
