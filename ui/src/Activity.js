import React from "react"

class Activity extends Component {
	state = {}
	render() {
		return (
			<div>
				<div>{this.props.activity.date}</div>
				<div>{this.props.activity.about}</div>
				<div>{this.props.activity.time}</div>
			</div>
		)
	}
}

export default Activity
