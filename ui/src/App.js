import React, { useState } from "react"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"

import Login from "./components/Login"
import Register from "./components/Register"
import UserDashboard from "./components/UserDashboard"
import ProfessorDashboard from "./components/ProfessorDashboard"
import AddActivity from "./components/AddActivity"
import Feedback from "./components/Feedback"

import axios from "axios"
function App() {
	const [user, setUser] = useState(null)
	function login(email, password, history) {
		axios.get(`http://localhost:8080/api/user/${email}`).then(user => {
			if (user.data && user.data.password === password) {
				setUser(user.data)
				if (user.data.isAdmin) {
					history.push("/dashboard/professor")
				} else {
					history.push("/dashboard/user")
				}
			} else {
				console.log("Try again")
			}
		})
	}

	return (
		<div>
			<Router>
				<Switch>
					<Route
						exact
						path={"/"}
						render={({ history }) => <Login history={history} login={login}></Login>}
					/>
					<Route
						exact
						path={"/register"}
						render={({ history }) => <Register history={history}></Register>}
					/>
					<Route
						exact
						path={"/dashboard/user"}
						render={({ history }) => <UserDashboard history={history} user={user}></UserDashboard>}
					/>
					<Route
						exact
						path={"/dashboard/professor"}
						render={({ history }) => (
							<ProfessorDashboard history={history} user={user}></ProfessorDashboard>
						)}
					/>
					<Route
						exact
						path={"/addActivity"}
						render={({ history }) => <AddActivity history={history} user={user}></AddActivity>}
					/>
					<Route
						path={"/feedback/:activityId"}
						render={({ history, ...rest }) => (
							<Feedback history={history} user={user} {...rest}></Feedback>
						)}
					/>
				</Switch>
			</Router>
		</div>
	)
}

export default App
