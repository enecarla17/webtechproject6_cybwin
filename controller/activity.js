const activityService = require("./../service/activity")

const getActivity = async (req, res) => {
	try {
		const activity = await activityService.getActivityByCode(req.params.code)
		res.status(200).send(activity)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const getActivityById = async (req, res) => {
	try {
		const activity = await activityService.getActivityById(req.params.id)
		res.status(200).send(activity)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const getActivities = async (req, res) => {
	try {
		const activities = await activityService.get(req.params.id)
		res.status(200).send(activities)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const createActivity = async (req, res, next) => {
	const activity = req.body
	if (activity.date && activity.code && activity.about && activity.time && activity.idAdmin) {
		await activityService.create(activity)
		res.status(201).send({
			message: "Activity created."
		})
	} else {
		res.status(400).send({
			message: "Wrong payload"
		})
	}
}

module.exports = {
	getActivities,
	createActivity,
	getActivityById,
	getActivity
}
