const userService = require("./../service/user")

const getUser = async (req, res) => {
	try {
		const user = await userService.getUserByEmail(req.params.email)
		res.status(200).send(user)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const createUser = async (req, res) => {
	const user = req.body
	if (user.firstname && user.lastname && user.email && user.isAdmin && user.idSpeciality) {
		await userService.create(user)
		res.status(201).send({
			message: "User created."
		})
	} else {
		res.status(400).send({
			message: "Wrong payload"
		})
	}
}

module.exports = {
	getUser,
	createUser
}
