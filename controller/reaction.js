const reactionService = require("./../service/reaction")

const getReactions = async (req, res) => {
	try {
		const reaction = await reactionService.getReactions()
		res.status(200).send(reaction)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

module.exports = {
	getReactions
}
