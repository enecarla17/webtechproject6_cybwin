const reactionBindService = require("./../service/reactionBind")

const getFeedback = async (req, res) => {
	try {
		const feedback = await reactionBindService.getFeedback(req.params.idActivity)
		res.status(200).send(feedback)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const createReactionBind = async (req, res) => {
	const reactionBind = req.body
	if (reactionBind.idActivity && reactionBind.idReaction) {
		await reactionBindService.create({ date: new Date(), ...reactionBind })
		res.status(201).send({
			message: "Reaction bind created."
		})
	} else {
		res.status(400).send({
			message: "Wrong payload"
		})
	}
}

module.exports = {
	createReactionBind,
	getFeedback
}
