const specialityService = require("./../service/speciality")

const getSpecialities = async (req, res) => {
	try {
		const speciality = await specialityService.getSpecialities()
		res.status(200).send(speciality)
	} catch (err) {
		res.status(500).send({
			message: `Error occured: ${err.message}`
		})
	}
}

const createSpeciality = async (req, res) => {
	const speciality = req.body
	if (speciality.name) {
		await specialityService.create(speciality)
		res.status(201).send({
			message: "Speciality created."
		})
	} else {
		res.status(400).send({
			message: "Wrong payload"
		})
	}
}

module.exports = {
	getSpecialities,
	createSpeciality
}
